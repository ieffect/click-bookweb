$(document).ready(function () {
    $('.nav-menu ul li:nth-of-type(1) a').click(function () {
        $(this).addClass("active-link");
        $('.nav-menu ul li:nth-of-type(2) a').removeClass("active-link");
        $('.nav-menu ul li:nth-of-type(3) a').removeClass("active-link");

    });

    $('.nav-menu ul li:nth-of-type(2) a').click(function () {
        $(this).addClass("active-link");
        $('.nav-menu ul li:nth-of-type(1) a').removeClass("active-link");
        $('.nav-menu ul li:nth-of-type(3) a').removeClass("active-link");

    });

    $('.nav-menu ul li:nth-of-type(3) a').click(function () {
        $(this).addClass("active-link");
        $('.nav-menu ul li:nth-of-type(2) a').removeClass("active-link");
        $('.nav-menu ul li:nth-of-type(1) a').removeClass("active-link");

    });


});

$(document).ready(function () {
    $('.nav-hamburger-menu').click(function () {
        $('.nav-menu').addClass('nav-menu-mobile');
        $('.nav-menu').toggleClass("disp")
    });
});


var wrapperMenu = document.querySelector('.wrapper-menu');

wrapperMenu.addEventListener('click', function () {
    wrapperMenu.classList.toggle('open');
})

$(document).ready(function() {
$('a[href^="#"]').click(function(){
var el = $(this).attr('href');
$('body').animate({
scrollTop: $(el).offset().top}, 500);
return false;
});
});